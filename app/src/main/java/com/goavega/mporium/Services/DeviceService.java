package com.goavega.mporium.Services;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;

import com.goavega.mporium.Activities.DashboardDisplayActivity;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Model.DeviceDetailModel;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.ManufacturerDetailModel;
import com.goavega.mporium.Model.ServiceProviderModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.AddDeviceTable;
import com.goavega.mporium.db.ManufacturerTable;
import com.goavega.mporium.db.MyDbOpenHelper;
import com.goavega.mporium.db.ServiceProviderTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by RAKESH on 1/11/2016.
 */
public class DeviceService  {
    private Context context;
    ApiHelper apiHelper;
    AddDeviceTable dbOperations;
    ApiUrls apiUrl;
    //StringBuilder warrantyScheduleDate;//serviceScheduleDate;
    String serviceScheduleDate,warrantyDate;
    Calendar purchaseDateCalendar;
    Date serviceDate;
    ConnectionDetector connectionDetector;

    long days,
            warrantyYears,
            warrantyDays,
            warrantyMonths;
    int     serviceScheduleDay,
            serviceScheduleMonth,
            serviceScheduleYear;
    public DeviceService(Context context) {
        this.context = context;
        apiUrl=new ApiUrls();
        apiHelper=new ApiHelper(context);
    }

    private void showToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public DeviceDetailModel setDeviceDetails(DeviceDetailModel deviceDetailModel)
    {
        apiHelper=new ApiHelper(context);
        JSONObject device;
        String deviceDetail;
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        try {
            device=new JSONObject();
            device.put("id", deviceDetailModel.getDeviceId());
            device.put("deviceName",deviceDetailModel.getDeviceName());
            String deviceId=device.toString();

            if(connectionDetector.isNetworkAvailable()) {

                deviceDetail = apiHelper.getUserDetails(deviceId, context.getString(R.string.url) + apiUrl.sendDeviceUrl);
                device = new JSONObject(deviceDetail);
                deviceDetailModel.setDeviceName(device.getString("deviceName"));
                deviceDetailModel.setPurchaseDate(device.getString(("dateOfPurchase")).substring(0, 10));
            /*if(!device.getString("modelNumber").equals("null"))*/
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                Date parsed = format.parse(device.getString("dateOfPurchase"));
                java.sql.Date sql = new java.sql.Date(parsed.getTime());
                cal.setTime(sql);
                cal.add(Calendar.DAY_OF_MONTH, 1);
                String formatted = format1.format(cal.getTime());
                //deviceDetailModel.setPurchaseDate(device.getString("dateOfPurchase"));

                if(!device.getString("modelNumber").equals("null"))

                    deviceDetailModel.setModelNumber(device.getString("modelNumber"));
                else
                    deviceDetailModel.setModelNumber("");
                //if(!device.getString("modelNumber").equals("null"))

                if(!device.getString("serialNumber").equals("null"))
                    deviceDetailModel.setSerialNumber(device.getString("serialNumber"));
                else
                    deviceDetailModel.setSerialNumber("");
                //else
                //deviceDetailModel.setSerialNumber("Not Set");
                deviceDetailModel.setWarrantyPeriod(device.getInt("warrantyPeriod"));
                deviceDetailModel.setManufacturerName(device.getString("manufacturerName"));
                deviceDetailModel.setServiceProviderName(device.getString("serviceProviderName"));
                deviceDetailModel.setServiceSchedulePeriod(device.getInt("frequencySchedule"));
                deviceDetailModel.setFrequencyService(device.getInt("serviceFrequency"));
                deviceDetailModel.setServiceProviderId(device.getLong("serviceProviderId"));
                if (!device.getString("serviceProviderCity").equals("null"))
                    deviceDetailModel.setServiceProviderCity(device.getString("serviceProviderCity"));
                else
                    deviceDetailModel.setServiceProviderCity("");
                if (!device.getString("serviceProviderPhone").equals("null"))
                    deviceDetailModel.setServviceProviderPhoneNumber(device.getString("serviceProviderPhone"));
                else
                    deviceDetailModel.setServviceProviderPhoneNumber("");
            }
            else
            {
                deviceDetailModel.setErrors(R.string.network_faliure);
            }


        }
        catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return deviceDetailModel;

    }


    public boolean sendDeviceData(DeviceDetailModel deviceDetailModel)
    {
        String serviceScheduleDates,warrantyScheduleDates;
        apiHelper=new ApiHelper(context);
        dbOperations = new AddDeviceTable(context);
        apiUrl=new ApiUrls();
        validate(deviceDetailModel);
        String userDetail;
        connectionDetector=new ConnectionDetector(context);
        String message;
        boolean success=false,isSaved=false,isInserted=false;
        if(!(deviceDetailModel.getHasErrors())) {

            if(!deviceDetailModel.getWarrantyPeriod().equals(0))
                warrantyScheduleCalculation(deviceDetailModel);

            deviceDetailModel.setWarrantyYears(warrantyYears);

            if(!deviceDetailModel.getServiceSchedulePeriod().equals(0))
                serviceScheduleCalculation(deviceDetailModel);



            deviceDetailModel.setServiceScheduleDate(serviceScheduleDate);
            deviceDetailModel.setWarrantyScheduleDates(warrantyDate);
            if(warrantyYears>=1)
                deviceDetailModel.setWarrantyYears(warrantyYears);
            else
                deviceDetailModel.setWarranty_remaining_days(warrantyDays);
            deviceDetailModel.setDays(days);
            JSONObject Device = new JSONObject();
            try {
                Device.put("deviceName", deviceDetailModel.getDeviceName());
                Device.put("purchaseDate", deviceDetailModel.getPurchaseDateDb());
                //Device.put("purchaseDate",deviceDetailModel.getPurchaseDate());
                Device.put("modelNumber", deviceDetailModel.getModelNumber());
                Device.put("serialNumber", deviceDetailModel.getSerialNumber());

                //  if(!deviceDetailModel.getWarrantyPeriod().equals(0))
                Device.put("warrantyPeriod",deviceDetailModel.getWarrantyPeriod());
                //if(!deviceDetailModel.getServiceSchedulePeriod().equals(0))
                Device.put("serviceSchedule",deviceDetailModel.getServiceSchedulePeriod());
                Device.put("user", SaveUserLogin.getUserId(context));
                Device.put("manufacturer",deviceDetailModel.getManufacturerId());
                Device.put("serviceProvider",deviceDetailModel.getServiceProviderId());
                Device.put("serviceFrequency",null);
                userDetail = Device.toString();

                if(connectionDetector.isNetworkAvailable()) {

                    message = apiHelper.sendUserDetail(userDetail, context.getString(R.string.url) + apiUrl.saveDeviceUrl);
                    Device = new JSONObject(message);
                    if (Device.getString("success").equals("true")) {
                        isSaved = true;
                        deviceDetailModel.setDeviceId(Device.getLong("deviceId"));
                        isInserted = dbOperations.insertDevice(deviceDetailModel);
                        if (isInserted) {
                            isSaved = true;
                        }
                    } else {
                        deviceDetailModel.setErrors(R.string.failure);
                    }
                }
                else
                {
                    deviceDetailModel.setErrors(R.string.network_faliure);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }

    public void validate(DeviceDetailModel deviceDetailModel)
    {
        deviceDetailModel.Errors.clear();
        if (deviceDetailModel.getDeviceName().equals(""))
            deviceDetailModel.setErrors(R.string.device_name_blank);
        if (deviceDetailModel.getPurchaseDate().equals(""))
            deviceDetailModel.setErrors(R.string.select_purchase_date);
        if (deviceDetailModel.getManufacturerName().equals(""))
            deviceDetailModel.setErrors(R.string.manufacturerNameBlank);

        if (deviceDetailModel.getServiceProviderName().equals(""))
            deviceDetailModel.setErrors(R.string.serviceProviderNameBlank);


        if(deviceDetailModel.getWarrantyPeriod()>12||deviceDetailModel.getWarrantyPeriod()<0)
            deviceDetailModel.setErrors(R.string.invalid_warranty_period);
        if(deviceDetailModel.getServiceSchedulePeriod()>12||deviceDetailModel.getServiceSchedulePeriod()<0)
            deviceDetailModel.setErrors(R.string.invalid_service_schedule);
    }


    public void warrantyScheduleCalculation(DeviceDetailModel deviceDetailModel)
    {
        Date warrantDATE;
        /*int warrantyScheduleDay,
            warrantyScheduleMonth,
            warrantyScheduleYear;*/
        purchaseDateCalendar=Calendar.getInstance();
        purchaseDateCalendar.setTime(deviceDetailModel.getPurchaseDateDb());
        int warrantyYeaar=deviceDetailModel.getWarrantyPeriod();
        purchaseDateCalendar.add(Calendar.YEAR, warrantyYeaar);
        warrantDATE=purchaseDateCalendar.getTime();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        warrantyDate = df.format(warrantDATE);
        Calendar warrantyCalender=Calendar.getInstance();
        warrantyCalender.setTime(warrantDATE);
        Calendar todayCalender=Calendar.getInstance();
        long diff = warrantyCalender.getTimeInMillis() - todayCalender.getTimeInMillis();
        warrantyDays = diff / (24 * 60 * 60 * 1000);

        warrantyYears = warrantyDays / 365;
    }

    public void serviceScheduleCalculation(DeviceDetailModel deviceDetailModel) {
        serviceScheduleMonth = deviceDetailModel.getServiceSchedulePeriod();
        purchaseDateCalendar=Calendar.getInstance();
        purchaseDateCalendar.setTime(deviceDetailModel.getPurchaseDateDb());
        purchaseDateCalendar.add(Calendar.MONTH, serviceScheduleMonth);
        serviceDate=purchaseDateCalendar.getTime();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        serviceScheduleDate = df.format(serviceDate);
        Calendar serviceScheduleCalender=Calendar.getInstance();
        serviceScheduleCalender.setTime(serviceDate);
        Calendar todayCalender=Calendar.getInstance();
        long diff = serviceScheduleCalender.getTimeInMillis() - todayCalender.getTimeInMillis();
        days = diff / (24 * 60 * 60 * 1000);


    }



    public ManufacturerDetailModel getManufacturerDetails(){
        apiUrl=new ApiUrls();
        ManufacturerDetailModel manufacturerDetailModel;
        manufacturerDetailModel=new ManufacturerDetailModel();
        String Manu = apiHelper.readAllUserDetails(context.getString(R.string.url)+apiUrl.getManufacturerUrl);

        JSONArray manufacturerJsonArray=null;
        JSONObject manufacturerJsonObject=null;
        try {
            manufacturerJsonObject = new JSONObject(Manu);
            manufacturerJsonArray = manufacturerJsonObject.getJSONArray("manufacturerDetails");
            for (int i = 0; i < manufacturerJsonArray.length(); i++) {
                manufacturerJsonObject = manufacturerJsonArray.getJSONObject(i);
                manufacturerDetailModel.setManufacturerId(manufacturerJsonObject.getLong("id"));
                manufacturerDetailModel.setName(manufacturerJsonObject.getString("manufacturerName"));
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return manufacturerDetailModel;
    }

    public ServiceProviderModel getServiceProviderDetails() {

        apiUrl=new ApiUrls();
        ServiceProviderModel serviceProviderModel;
        serviceProviderModel=new ServiceProviderModel();
        JSONArray serviceProviderJsonArray=null;
        JSONObject serviceProviderObject=null;
        try {
            String serviceProviderDetails =apiHelper.readAllUserDetails(context.getString(R.string.url)+apiUrl.getServiceProviderUrl);
            serviceProviderObject = new JSONObject(serviceProviderDetails);
            serviceProviderJsonArray = serviceProviderObject.getJSONArray("serviceProviderDetails");
            for (int i = 0; i < serviceProviderJsonArray.length(); i++) {
                serviceProviderObject = serviceProviderJsonArray.getJSONObject(i);
                serviceProviderModel.setServiceProviderId(serviceProviderObject.getLong("serviceProviderId"));
                serviceProviderModel.setName(serviceProviderObject.getString("serviceProviderName"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return serviceProviderModel;
    }

    public boolean deviceUpdate(DeviceDetailModel deviceDetailModel)
    {
        JSONObject device;
        apiHelper=new ApiHelper(context);
        apiUrl=new ApiUrls();
        boolean isUpdated = false;
        String deviceDetail;
        try {
            device = new JSONObject();
            device.put("serviceSchedule", deviceDetailModel.getServiceSchedulePeriod());
            device.put("warrantyPeriod", deviceDetailModel.getWarrantyPeriod());
            device.put("serviceProviderName",deviceDetailModel.getServiceProviderName());
            device.put("id", deviceDetailModel.getDeviceId());
            String deviceUpdate = device.toString();
            deviceDetail = apiHelper.sendUpdateUserDetails(deviceUpdate,context.getString(R.string.url)+apiUrl.serviceScheduleUpdateUrl);
            device = new JSONObject(deviceDetail);
            if (device.getString("success").equals("true")) {
                isUpdated=true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isUpdated;
    }


}

