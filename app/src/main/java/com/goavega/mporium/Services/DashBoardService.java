package com.goavega.mporium.Services;

import android.content.Context;
import android.widget.Toast;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.DashBoardModel;
import com.goavega.mporium.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by RAKESH on 2/1/2016.
 */
public class DashBoardService {
    private Context context;
    ApiHelper apiHelper;
    ApiUrls apiUrl;
    DashBoardModel dashBoardModel;
    ConnectionDetector connectionDetector;

    public DashBoardService(Context context) {
        this.context = context;
    }
    private void showToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public ArrayList<DashBoardModel> setDeviceDetails(){
        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        //dashBoardModel=new DashBoardModel();
        connectionDetector = new ConnectionDetector(context);
        String message, deviceDetail;
        String serviceScheduleDate,warrantyPeriodDate;
        ArrayList<DashBoardModel> dashBoardList = new ArrayList<DashBoardModel>();
        JSONObject device = new JSONObject();
        JSONArray deviceJsonArray=new JSONArray();
        try {
            device.put("user", SaveUserLogin.getUserId(context));
            deviceDetail = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.dashBoardDisplayUrl);
                device = new JSONObject(message);
                deviceJsonArray = device.getJSONArray("calculationDetails");
                if (device.getString("success").equals("true")) {
                for (int i = 0; i < deviceJsonArray.length(); i++) {
                    device = deviceJsonArray.getJSONObject(i);
                    dashBoardModel=new DashBoardModel();
                    dashBoardModel.setDeviceName(device.getString("deviceName"));
                    dashBoardModel.setDeviceId(device.getLong("deviceId"));
                    dashBoardModel.setUserId(SaveUserLogin.getUserId(context));
                    dashBoardModel.setPurchaseDate((device.getString("purchaseDate")).substring(0, 10));
                    dashBoardModel.setWarrantyPeriods(device.getInt("warrantyPeriod"));
                    dashBoardModel.setManufacturerName(device.getString("manufacturerName"));
                    dashBoardModel.setServiceSchedulePeriod(device.getInt("serviceSchedule"));
                    dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                    dashBoardModel.setWarrantyPeriodExpire((device.getString("warrantyDate")).substring(0, 10));
                    dashBoardModel.setWarrantyRemainingDays(device.getInt("warrantyRemaining"));
                    dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining"));
                    //dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                    dashBoardModel.setColor("#FF0000");
                    int serviceRemainingDaysInt = device.getInt("serviceRemaining");
                    // Toast.makeText(getApplicationContext(),"serviceperiod"+serviceRemainingDaysInt,Toast.LENGTH_LONG).show();
                    if (serviceRemainingDaysInt>0) {
                       // dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining")). + "Days Left");
                        if (serviceRemainingDaysInt<10)
                        {
                            dashBoardModel.setColor("#FF0000");
                        }
                        if((serviceRemainingDaysInt>10)||(serviceRemainingDaysInt<20))
                        {
                            dashBoardModel.setColor("#F1A006");
                        }
                        if(serviceRemainingDaysInt>20)
                        {
                            dashBoardModel.setColor("#00a300");
                        }

                    }

                    if (serviceRemainingDaysInt<0){

                        dashBoardModel.setColor("#FF0000");

                    }
                    dashBoardList.add(dashBoardModel);

                }

                } else {
                    dashBoardModel.setErrors(R.string.data_available);
                }
            } else {
                dashBoardModel.setErrors(R.string.network_faliure);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dashBoardList;
    }


    public ArrayList<DashBoardModel> sendDateFilterData(int datePickerMonthValue,int datePickerYearValue){
        JSONObject device;
        JSONArray deviceJsonArray;
        String deviceDetail;
        connectionDetector = new ConnectionDetector(context);
        ArrayList<DashBoardModel> dashBoardList = new ArrayList<DashBoardModel>();
        try {
            device = new JSONObject();
            deviceJsonArray=new JSONArray();
            device.put("month", datePickerMonthValue);
            device.put("year", datePickerYearValue);
            device.put("user",SaveUserLogin.getUserId(context));
            String deviceId = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                deviceDetail = apiHelper.sendUserDetail(deviceId, context.getString(R.string.url) + apiUrl.dateFilterUrl);
                device = new JSONObject(deviceDetail);
                deviceJsonArray = device.getJSONArray("dateFilter");
                if (device.getString("success").equals("true")) {
                    for (int i = 0; i < deviceJsonArray.length(); i++) {
                        device = deviceJsonArray.getJSONObject(i);
                        dashBoardModel = new DashBoardModel();
                        dashBoardModel.setDeviceName(device.getString("deviceName"));
                        dashBoardModel.setDeviceId(device.getLong("deviceId"));
                        dashBoardModel.setUserId(SaveUserLogin.getUserId(context));
                        dashBoardModel.setPurchaseDate((device.getString("purchaseDate")).substring(0, 10));
                        dashBoardModel.setWarrantyPeriods(device.getInt("warrantyPeriod"));
                        dashBoardModel.setManufacturerName(device.getString("manufacturerName"));
                        dashBoardModel.setServiceSchedulePeriod(device.getInt("serviceSchedule"));
                        //dashBoardModel.setServiceScheduleDate(device.getString("serviceScheduleDate"));
                        dashBoardModel.setWarrantyPeriodExpire((device.getString("warrantyDate")).substring(0, 10));
                        dashBoardModel.setWarrantyRemainingDays(device.getInt("warrantyRemaining"));
                        dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining"));
                        dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                        dashBoardModel.setColor("#FF0000");
                        int serviceRemainingDaysInt = device.getInt("serviceRemaining");
                        // Toast.makeText(getApplicationContext(),"serviceperiod"+serviceRemainingDaysInt,Toast.LENGTH_LONG).show();
                        if (serviceRemainingDaysInt > 0) {
                            // dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining")). + "Days Left");
                            if (serviceRemainingDaysInt < 10) {
                                dashBoardModel.setColor("#FF0000");
                            }
                            if ((serviceRemainingDaysInt > 10) || (serviceRemainingDaysInt < 20)) {
                                dashBoardModel.setColor("#F1A006");
                            }
                            if (serviceRemainingDaysInt > 20) {
                                //dashBoardModel.setColor("#8B4513");
                                dashBoardModel.setColor("#00a300");
                            }

                        }

                        if (serviceRemainingDaysInt < 0) {

                            dashBoardModel.setColor("#FF0000");

                        }
                        dashBoardList.add(dashBoardModel);

                    }
                }
            }
            else
            {
                dashBoardModel.setErrors(R.string.network_faliure);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dashBoardList;
    }

    public boolean checkDevicePresent()
    {
        boolean isPresent=false;
        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        dashBoardModel=new DashBoardModel();
        connectionDetector = new ConnectionDetector(context);
        String message, deviceDetail;
        JSONObject device = new JSONObject();
         try {
            device.put("user", SaveUserLogin.getUserId(context));
            deviceDetail = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.getDeviceId);
                device = new JSONObject(message);
                 if (device.getString("success").equals("true"))
                     isPresent=true;
              }
            else

            {
                dashBoardModel.setErrors(R.string.network_faliure);
                showToast("network failure");
                //isPresent=false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    return isPresent;
    }


    public boolean sendDeviceId(long deleteDeviceId) {
        JSONObject device;
        boolean isRemoved=false;
        String deviceDetail;
        try {
            device = new JSONObject();
            device.put("id", deleteDeviceId);
            device.put("isActive",false);
            String deviceId = device.toString();
            deviceDetail = apiHelper.sendUpdateUserDetails(deviceId,context.getString(R.string.url) + apiUrl.deleteDeviceUrl);
            device = new JSONObject(deviceDetail);
            if (device.getString("success").equals("true")) {
                isRemoved=true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isRemoved;
    }
}



