package com.goavega.mporium.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.os.StrictMode;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.RateModel;
import com.goavega.mporium.R;
import com.goavega.mporium.Services.RateService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FeedBackActivity extends AppCompatActivity {
    RatingBar ratingBar;
    Button submitButton;
    RateService rateService;
    float serviceProviderRate;
    String userComment,
            manufacturerName,
            deviceName;
    RateModel rateModel;
    EditText commentText;
    TextView userReplyText,
            deviceNameText,
            serviceProviderNameText,
            manufacturerNameText,
            suggestionText,
            cancelText;

    TextView custom_message;
    Toast toast;
    View layout;

    Long deviceId,
            serviceProviderId;
    ImageView backArrowImage;
    Calendar calendar;
    Boolean success=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        backArrowImage=(ImageView)findViewById(R.id.backArrow);
        ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        submitButton=(Button)findViewById(R.id.submitButton);
        commentText=(EditText)findViewById(R.id.commentText);
        userReplyText=(TextView)findViewById(R.id.userReplyText);
        deviceNameText=(TextView)findViewById(R.id.deviceNameTexts);
        manufacturerNameText=(TextView)findViewById(R.id.manufacturerNameText);
        serviceProviderNameText=(TextView)findViewById(R.id.serviceProviderNameTexts);
        suggestionText=(TextView)findViewById(R.id.suggestionText);
        cancelText = (TextView)findViewById(R.id.cancelText);

        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        custom_message=(TextView)layout.findViewById(R.id.messageText);


        Intent calledIntent = getIntent();
        Bundle extras = calledIntent.getExtras();
        rateModel=new RateModel();
        rateService=new RateService(FeedBackActivity.this);
        if(extras != null)
            deviceId = extras.getLong("deviceId");

        RelativeLayout touchInterceptor = (RelativeLayout)findViewById(R.id.relativeMain);
        touchInterceptor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (commentText.isFocused()) {
                        Rect outRect = new Rect();
                        commentText.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            commentText.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });

        rateModel.setUserId(SaveUserLogin.getUserId(FeedBackActivity.this));
        rateModel.setDeviceId(deviceId);
        rateModel=rateService.getRatingDetails(rateModel);
        serviceProviderId=rateModel.getServiceProviderId();
        manufacturerName=rateModel.getManufacturerName();
        deviceName=rateModel.getDeviceName();
        deviceNameText.setText(rateModel.getDeviceName());
        serviceProviderNameText.setText(rateModel.getServiceProviderName());
        manufacturerNameText.setText("-"+rateModel.getManufacturerName());
        serviceProviderRate=ratingBar.getRating();

        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                serviceProviderRate=ratingBar.getRating();
                if(serviceProviderRate==1)
                {
                    userReplyText.setText(R.string.very_bad);
                    suggestionText.setText(R.string.went_wrong);
                }
                else if (serviceProviderRate==2)
                {
                    userReplyText.setText(R.string.bad_one);
                    suggestionText.setText(R.string.went_wrong);
                }
                else if (serviceProviderRate==3)
                {
                    userReplyText.setText(R.string.ok_service);
                    suggestionText.setText(R.string.went_wrong);
                }
                else if (serviceProviderRate==4)
                {
                    userReplyText.setText(R.string.good_service);
                    suggestionText.setText("");
                }
                else
                {
                    userReplyText.setText(R.string.great_service);
                    suggestionText.setText("");
                }

            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                Date today = Calendar.getInstance().getTime();
                String todayDate = df.format(today).substring(0, 10);
                serviceProviderRate=ratingBar.getRating();
                userComment=commentText.getText().toString();
                rateModel.setUserId(SaveUserLogin.getUserId(FeedBackActivity.this));
                rateModel.setDeviceId(deviceId);
                rateModel.setUserComment(userComment);
                rateModel.setServiceProviderRate(serviceProviderRate);
                rateModel.setServiceProviderId(serviceProviderId);
                rateModel.setDeviceName(deviceName);
                rateModel.setServiceDate(todayDate);
                rateModel.setManufacturerName(manufacturerName);
                success=rateService.sendRatingDetails(rateModel);
                if(success)
                {
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message.setText(R.string.rateAdded);
                    toast.show();


                    //Toast.makeText(getApplicationContext(),R.string.rateAdded,Toast.LENGTH_LONG).show();
                    Intent gotoDashBoard=new Intent(getApplicationContext(),DashboardDisplayActivity.class);
                    startActivity(gotoDashBoard);
                }
                else
                {
                    layout.setBackgroundResource(R.color.lightRed);
                    custom_message.setText(R.string.update_failure);
                    toast.show();

                    //Toast.makeText(getApplicationContext(),R.string.update_failure,Toast.LENGTH_LONG).show();
                }
           }
        });
        backArrowImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

}
