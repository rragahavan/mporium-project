package com.goavega.mporium.Activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.DashBoardModel;
import com.goavega.mporium.R;
import com.goavega.mporium.Services.DashBoardService;
import com.goavega.mporium.db.AddDeviceTable;
import com.goavega.mporium.db.MyDbOpenHelper;


public class DashBoardActivity extends BaseActivity {
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    ImageView menuIconImage;
    DashBoardModel dashBoardModel;
    boolean isDeviceExist=false;
    AddDeviceTable deviceTable;
    Button addDeviceButton;
    DashBoardService dashBoardService;
    boolean isPresent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        menuIconImage = (ImageView) findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        addDeviceButton=(Button)findViewById(R.id.addDeviceButton);
        addDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoAddDevice = new Intent(DashBoardActivity.this, AddDeviceActivity.class);
                startActivity(gotoAddDevice);
            }
        });
        /*String userId=SaveUserLogin.getUserId(DashBoardActivity.this).toString();
        deviceTable=new AddDeviceTable(DashBoardActivity.this);
        Cursor c=deviceTable.readAllDevices();
        if(c!=null&&c.getCount()>0)
        {
            while (c.moveToNext()) {
                if ((c.getString(c.getColumnIndex(MyDbOpenHelper.deviceUserId))).equals(userId)) {
                    Intent gotoDashBoardDisplay = new Intent(DashBoardActivity.this, DashboardDisplayActivity.class);
                    startActivity(gotoDashBoardDisplay);
                    finish();
                }
            }
        }
*/


        dashBoardService = new DashBoardService(DashBoardActivity.this);
        isPresent=dashBoardService.checkDevicePresent();
        if(isPresent) {
            Intent gotoDashBoardDisplay = new Intent(DashBoardActivity.this, DashboardDisplayActivity.class);
            startActivity(gotoDashBoardDisplay);
            finish();
        }

    }

}