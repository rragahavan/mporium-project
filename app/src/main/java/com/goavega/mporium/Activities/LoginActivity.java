package com.goavega.mporium.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Activities.ServiceProviderDashBoardActivity;
import com.goavega.mporium.Services.LoginService;

public class LoginActivity extends AppCompatActivity {
    TextView signUpLinkText;
    Button loginButton;
    EditText passwordText,
            emailAddressText;
    LoginService loginService;
    boolean isValid=false;
    UserDetailModel userDetailModel;
    TextView forgotPasswordText;
    boolean isPresent=false;
    String emailAddress;
    String userType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(SaveUserLogin.getUserId(LoginActivity.this) != 0)
        {
            Intent gotoDashBoard = new Intent(LoginActivity.this,DashBoardActivity.class);
            startActivity(gotoDashBoard);
            finish();
        }

        //todays change
        /*if(SaveUserLogin.getPrefServiceProviderId(LoginActivity.this)!=0)
        {
            Intent gotoServiceProviderDashBoard = new Intent(LoginActivity.this,ServiceProviderDashBoardActivity.class);
            startActivity(gotoServiceProviderDashBoard);
            finish();
        }*/


        Intent calledIntent = getIntent();
        Bundle extras = calledIntent.getExtras();
        if(extras != null) {
            userType = extras.getString("userType");
        }




        StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        loginService=new LoginService(LoginActivity.this);
        userDetailModel=new UserDetailModel();
        passwordText=(EditText)findViewById(R.id.passwordText);
        emailAddressText=(EditText)findViewById(R.id.emailAddressText);
        forgotPasswordText=(TextView)findViewById(R.id.forgotText);
        signUpLinkText=(TextView)findViewById(R.id.signUpLinkText);
        loginButton=(Button)findViewById(R.id.loginButton);

        forgotPasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = getLayoutInflater();
                View layout = li.inflate(R.layout.customtoast,
                        (ViewGroup) findViewById(R.id.custom_toast_layout));
                Toast toast = new Toast(getApplicationContext());
                layout.setBackgroundResource(R.color.lightRed);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
                toast.setView(layout);
                emailAddress=emailAddressText.getText().toString();
                if(emailAddress.equals(""))
                {
                    toast.show();
                }
                else
                {
                    final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                            "Please wait...", true);
                    dialog.show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 8000);
                    TextView custom_message=(TextView)layout.findViewById(R.id.forgotMessage);
                    isPresent=loginService.forgotPassword(emailAddress);
                    if(isPresent)
                    {
                        layout.setBackgroundResource(R.color.lightGreen);
                        custom_message.setText(R.string.reset_password);
                        toast.show();
                    }
                    else {
                        dialog.dismiss();
                        custom_message.setText(R.string.accountNotExist);
                        toast.show();
                    }
                }
            }
        });

        signUpLinkText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoSignUp = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(gotoSignUp);
                overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left);
            }
        });

        RelativeLayout touchInterceptor = (RelativeLayout)findViewById(R.id.relativeMain);
        touchInterceptor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (emailAddressText.isFocused() || passwordText.isFocusable()) {
                        Rect outRect = new Rect();
                        emailAddressText.getGlobalVisibleRect(outRect);
                        passwordText.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            emailAddressText.clearFocus();
                            passwordText.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);

                LayoutInflater li = getLayoutInflater();
                View layout = li.inflate(R.layout.success_message_display,
                        (ViewGroup) findViewById(R.id.message_display));
                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP,0,0);
                toast.setView(layout);
                userDetailModel.setEmailAddress(emailAddressText.getText().toString().trim());
                userDetailModel.setPassword(passwordText.getText().toString());

                /*//today
                userDetailModel.setUserType(userType);
*/

                isValid = loginService.sendUserData(userDetailModel);
                if (isValid) {

                    SaveUserLogin.setPrefUserEmail(getApplicationContext(),userDetailModel.getEmailAddress());
                    Intent gotoDashBoard = new Intent(LoginActivity.this, DashBoardActivity.class);
                    startActivity(gotoDashBoard);

                } else {
                    if(userDetailModel.getErrors().size()!=0) {

                        layout.setBackgroundResource(R.color.lightRed);
                        TextView custom_message = (TextView) layout.findViewById(R.id.messageText);
                        custom_message.setText(userDetailModel.getErrors().get(0));
                        toast.show();
                    }    //Toast.makeText(LoginActivity.this, userDetailModel.getErrors().get(i), Toast.LENGTH_LONG).show();

                }
            }

        });
    }

}
