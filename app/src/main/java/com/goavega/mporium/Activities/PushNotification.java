package com.goavega.mporium.Activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.goavega.mporium.Classes.QuickstartPreferences;
import com.goavega.mporium.Classes.RegistrationIntentService;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.NotificationModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Activities.ServiceProviderDashBoardActivity;
import com.goavega.mporium.Services.NotificationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class PushNotification extends BaseActivity implements NumberPicker.OnValueChangeListener {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;
    int f=0;
    TextView daysText;
    ImageView selectDays;
    int appSettings=0;
    Boolean checked=true;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    ImageView menuIconImage;
    SwitchCompat notificationSwitch;
    NotificationService notificationService;
    NotificationModel notificationModel;
    boolean isCustomer;
    TextView custom_message;
    Toast toast;
    View layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_notification);
        notificationModel=new NotificationModel();
        menuIconImage=(ImageView)findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        custom_message=(TextView)layout.findViewById(R.id.messageText);

        Intent calledIntent = getIntent();
        Bundle extras = calledIntent.getExtras();
        if(extras != null) {
            isCustomer = extras.getBoolean("isCustomer");
        }



        daysText=(TextView) findViewById(R.id.daysText);

        notificationSwitch= (SwitchCompat) findViewById(R.id
                .switch1);

        selectDays=(ImageView)findViewById(R.id.downArrow);
        selectDays.setClickable(true);
        notificationSwitch.setChecked(false);

        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checked = true;
                    notificationModel.setNotification(true);
                    daysText.getText().toString();
                } else {
                    checked = false;
                    notificationModel.setNotification(false);
                    notificationModel.setUserId(SaveUserLogin.getUserId(getApplicationContext()));
                    notificationModel.setRemainingDays(Integer.parseInt(daysText.getText().toString()));
                }

            }
        });
        selectDays.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!checked) {
                    Toast.makeText(getApplicationContext(), "Notification button is off", Toast.LENGTH_LONG).show();
                } else {
                              show();
                    notificationModel.setRemainingDays(Integer.parseInt(daysText.getText().toString()));
                }
            }
        });
             mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
        mInformationTextView = (TextView) findViewById(R.id.informationTextView);
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                layout.setBackgroundResource(R.color.lightRed);
                custom_message.setText(R.string.device_not_support);
                toast.show();

                //Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
    public void show()
    {
        final Dialog d = new Dialog(PushNotification.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog);
        TextView b1 = (TextView) d.findViewById(R.id.addText);
        TextView b2 = (TextView) d.findViewById(R.id.cancelText);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(15);
        np.setMinValue(1);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(this);
        b1.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      daysText.setText(String.valueOf(np.getValue()));
                                      notificationModel.setRemainingDays(Integer.parseInt(daysText.getText().toString()));
                                      appSettings = 1;
                                      if (checkPlayServices()) {
                                          if (appSettings == 1) {
                        /*layout.setBackgroundResource(R.color.lightRed);
                        custom_message.setText(R.string.selected_days);
                        toast.show();
*/
                                              Toast.makeText(getApplicationContext(), "selected no.of days:-" + notificationModel.getRemainingDays(), Toast.LENGTH_LONG).show();
                                              Intent intent = new Intent(PushNotification.this, RegistrationIntentService.class);
                                              intent.putExtra("days", Integer.parseInt(daysText.getText().toString()));
                                              intent.putExtra("notification", true);
                                              intent.putExtra("isCustomer",isCustomer);
                                              if (isCustomer)
                                                  intent.putExtra("userId", SaveUserLogin.getUserId(getApplicationContext()));
                                              else
                                                  intent.putExtra("serviceProviderId", SaveUserLogin.getPrefServiceProviderId(getApplicationContext()));


                                              startService(intent);
                                              if (isCustomer) {
                                                  Intent gotoDashboard = new Intent(PushNotification.this, DashboardDisplayActivity.class);
                                                  startActivity(gotoDashboard);
                                              } else {
                                                  Intent gotoDashboard = new Intent(PushNotification.this, ServiceProviderDashBoardActivity.class);
                                                  startActivity(gotoDashboard);


                                              }

                                          }
                                      }
                                      d.dismiss();
                                  }
                              }

        );

            b2.setOnClickListener(new View.OnClickListener()

                                  {
                                      @Override
                                      public void onClick(View v) {
                                          d.dismiss();
                                      }
                                  }

            );
            d.show();
        }

    }




