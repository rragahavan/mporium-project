package com.goavega.mporium.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.Services.SignUpService;

public class SignUpActivity extends AppCompatActivity {
    EditText firstNameText,
             emailAddressText,
             //cityText,
             passwordText,
             confirmPasswordText;
    Button signUpButton;
    AutoCompleteTextView cityText;
    Boolean isSaved=false;
    SignUpService signUpService;
    ImageView backArrow;
    UserDetailModel userDetailModel;
    TextView custom_message;
    Toast toast;
    View layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        userDetailModel = new UserDetailModel();
        signUpService = new SignUpService(SignUpActivity.this);
        backArrow = (ImageView) findViewById(R.id.backArrowImage);
        signUpButton = (Button) findViewById(R.id.signUpButton);
        firstNameText = (EditText) findViewById(R.id.firstNameText);
        emailAddressText = (EditText) findViewById(R.id.emailIdText);
        cityText = (AutoCompleteTextView) findViewById(R.id.cityText);
        passwordText = (EditText) findViewById(R.id.passwordText);
        confirmPasswordText = (EditText) findViewById(R.id.confirmPasswordText);
        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);

        ArrayAdapter userCityAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.cities));
        cityText.setAdapter(userCityAdapter);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoLogin = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(gotoLogin);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager keyboard = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
                userDetailModel.setName(firstNameText.getText().toString());
                userDetailModel.setEmailAddress(emailAddressText.getText().toString().trim());
                userDetailModel.setCity(cityText.getText().toString());
                userDetailModel.setPassword(passwordText.getText().toString());
                userDetailModel.setConfirmPassword(confirmPasswordText.getText().toString());
                isSaved = signUpService.sendUserData(userDetailModel);
                if(isSaved)
                {
                    SaveUserLogin.setPrefUserEmail(getApplicationContext(), userDetailModel.getEmailAddress());
                    SaveUserLogin.setUserName(getApplicationContext(), userDetailModel.getName());
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText(R.string.registration_success);
                    toast.show();

                  //  Toast.makeText(SignUpActivity.this, R.string.registration_success, Toast.LENGTH_LONG).show();
                    Intent gotoDashBoard = new Intent(SignUpActivity.this, DashBoardActivity.class);
                    startActivity(gotoDashBoard);
                }
                else {
                    if(userDetailModel.getErrors().size()!=0) {

                        layout.setBackgroundResource(R.color.lightGreen);
                        custom_message = (TextView) layout.findViewById(R.id.messageText);
                        custom_message.setText(userDetailModel.getErrors().get(0));
                        toast.show();
                    }
                    /*for(int i=0;i<userDetailModel.getErrors().size();i++)
                    {
                        Toast.makeText(SignUpActivity.this,userDetailModel.getErrors().get(i), Toast.LENGTH_LONG).show();
                    }*/
                }
            }

        });
    }
}