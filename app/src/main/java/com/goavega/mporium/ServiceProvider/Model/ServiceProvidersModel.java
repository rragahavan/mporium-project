package com.goavega.mporium.ServiceProvider.Model;

import com.goavega.mporium.Classes.BaseModel;

/**
 * Created by Administrator on 30-03-2016.
 */
public class ServiceProvidersModel extends BaseModel{
    String  name,
            addressLine1,
            addressLine2,
            emailAddress,
            city,
            phoneNumber,
            password,
            confirmPassword,
            mobileNumber;
    Integer serviceProviderId;

    public boolean isServiceProviderLogin() {
        return serviceProviderLogin;
    }

    public void setServiceProviderLogin(boolean serviceProviderLogin) {
        this.serviceProviderLogin = serviceProviderLogin;
    }

    boolean  serviceProviderLogin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Integer serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }
}
