package com.goavega.mporium.ServiceProvider.Model;

import com.goavega.mporium.Classes.BaseModel;

/**
 * Created by Administrator on 06-04-2016.
 */
public class ServiceProviderDashboardModel extends BaseModel{
    String deviceName;
    Integer serviceRemainingDays;
    String ServiceScheduleDate;

    public Long getServiceProvideId() {
        return serviceProvideId;
    }

    public void setServiceProvideId(Long serviceProvideId) {
        this.serviceProvideId = serviceProvideId;
    }

    Long serviceProvideId;
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    String customerName;
    String customerCity;

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    String customerMobileNumber;
    Long userId;
    Integer serviceSchedulePeriod;

    public Integer getServiceSchedulePeriod() {
        return serviceSchedulePeriod;
    }

    public void setServiceSchedulePeriod(Integer serviceSchedulePeriod) {
        this.serviceSchedulePeriod = serviceSchedulePeriod;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Integer getServiceRemainingDays() {
        return serviceRemainingDays;
    }

    public void setServiceRemainingDays(Integer serviceRemainingDays) {
        this.serviceRemainingDays = serviceRemainingDays;
    }

    public String getServiceScheduleDate() {
        return ServiceScheduleDate;
    }

    public void setServiceScheduleDate(String serviceScheduleDate) {
        ServiceScheduleDate = serviceScheduleDate;
    }


    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getServiceScheduleExpire() {
        return serviceScheduleExpire;
    }

    public void setServiceScheduleExpire(String serviceScheduleExpire) {
        this.serviceScheduleExpire = serviceScheduleExpire;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    //String purchaseDate;
    //String manufacturerName;
    String serviceScheduleExpire;
    //String warrantyPeriodExpire;
    //Integer warrantyRemainingDays;
    String color;
    Long deviceId;
}
