package com.goavega.mporium.ServiceProvider.Activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.goavega.mporium.Activities.AddDeviceActivity;
import com.goavega.mporium.Activities.DashBoardActivity;
import com.goavega.mporium.Activities.DashboardDisplayActivity;
import com.goavega.mporium.Activities.LoginActivity;
import com.goavega.mporium.Activities.PushNotification;
import com.goavega.mporium.Activities.UpdateProfileActivity;
import com.goavega.mporium.Classes.DeviceDetector;
import com.goavega.mporium.Classes.NavDrawerItem;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.R;
import com.goavega.mporium.adapters.NavDrawerListAdapter;
import com.goavega.mporium.db.AddDeviceTable;
import com.goavega.mporium.db.MyDbOpenHelper;

import java.util.ArrayList;

public class NavigationDrawerActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    protected RelativeLayout _completeLayout, _activityLayout;
    private CharSequence mDrawerTitle;
    Toolbar toolbar;
    private CharSequence mTitle;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private ActionBarDrawerToggle mDrawerToggle;
    AddDeviceTable deviceTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
      
    }
    public void set(String[] navMenuTitles, TypedArray navMenuIcons) {
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.header_list, null, false);
        TextView profileNameText=(TextView)listHeaderView.findViewById(R.id.profileNameText);
        TextView profileEmailText=(TextView)listHeaderView.findViewById(R.id.profileEmailText);
        profileNameText.setText(SaveUserLogin.getServiceProviderName(getApplicationContext()));
        profileEmailText.setText(SaveUserLogin.getServiceProviderEmail(getApplicationContext()));
        mDrawerList.addHeaderView(listHeaderView);
        navDrawerItems = new ArrayList<NavDrawerItem>();
        if (navMenuIcons == null) {
            for (int i = 0; i < navMenuTitles.length; i++) {
                navDrawerItems.add(new NavDrawerItem(navMenuTitles[i]));
            }
        } else {
            for (int i = 0; i < navMenuTitles.length; i++) {
                navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons.getResourceId(i, -1)));
            }
        }

        //size of the navigation drawer half of the screen
        DeviceDetector deviceDetector;
        deviceDetector=new DeviceDetector(NavigationDrawerActivity.this);
        if(deviceDetector.isTablet(NavigationDrawerActivity.this)) {
            int width = getResources().getDisplayMetrics().widthPixels / 2;
            DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
            params.width = width;
            mDrawerList.setLayoutParams(params);
        }

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar,R.string.app_name,R.string.app_name
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();
            }

        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    private void displayView(int position) {
        Intent intent;
        boolean devicePresent=false;
        switch (position) {
            case 1:
                intent = new Intent(this, ServiceProviderDashBoardActivity.class);
                startActivity(intent);

                break;
            case 2:
                intent = new Intent(this,AddCustomerActivity.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, ServiceProviderUpdateProfile.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, ReviewServiceActivity.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this,PushNotification.class);

                intent.putExtra("isCustomer",false);


                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, ServiceProviderLoginActivity.class);
                SaveUserLogin.setServiceProviderName(NavigationDrawerActivity.this,"");
                SaveUserLogin.setServiceProviderEmail(NavigationDrawerActivity.this, "");
                SaveUserLogin.setPrefServiceProviderId(NavigationDrawerActivity.this, 0L);
                startActivity(intent);
                break;

            default:
                break;
        }
        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}