package com.goavega.mporium.ServiceProvider.Services;

import android.content.Context;

import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProvidersModel;
import com.goavega.mporium.db.UserProfileTable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 30-03-2016.
 */
public class ServiceProvidersSignUpService {

    private Context context;
    ApiHelper apiHelper;
   // ServiceProviderTable dbOperations;
//    ApiUrls apiUrl;
  //  boolean isSaved=false,isInserted;
    UserProfileTable dbOperations;
    ApiUrls apiUrl;
    boolean isSaved=false,isInserted=false;
    ConnectionDetector connectionDetector;


    public ServiceProvidersSignUpService(Context context){
        this.context = context;
    }
    public boolean sendUserData(ServiceProvidersModel serviceProvidersModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        validate(serviceProvidersModel);
        String userDetail;
        String message,uid;
        Long userId;

        if(!(serviceProvidersModel.getHasErrors())) {

            JSONObject User = new JSONObject();
            try {
                User.put("serviceProviderName", serviceProvidersModel.getName());
                User.put("emailId", serviceProvidersModel.getEmailAddress());
                User.put("serviceProviderCity", serviceProvidersModel.getCity());
                User.put("password", serviceProvidersModel.getPassword());
                User.put("serviceProviderPhoneNumber",serviceProvidersModel.getPhoneNumber());
                //User.put("serviceProviderPresent",serviceProvidersModel.isServiceProviderLogin());

                userDetail = User.toString();
                if(connectionDetector.isNetworkAvailable()) {

                    message = apiHelper.sendUserDetail(userDetail, context.getString(R.string.url)+apiUrl.serviceProviderSignUpUrl);

                    User = new JSONObject(message);
                    if (User.getString("success").equals("true")) {
                        //isInserted = dbOperations.insertUser(serviceProvidersModel);
                       // if (isInserted) {
                           // SaveUserLogin.setUserName(context, serviceProvidersModel.getEmailAddress());
                            SaveUserLogin.setPrefServiceProviderId(context, User.getLong("serviceProviderId"));
                            isSaved = true;
                       // }
                    } else {
                        serviceProvidersModel.setErrors(R.string.emailExists);
                        serviceProvidersModel.setErrors(R.string.failure);
                    }
                }
                else {
                    serviceProvidersModel.setErrors(R.string.network_faliure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }

    public void validate(ServiceProvidersModel serviceProvidersModel) {
        serviceProvidersModel.Errors.clear();
        if (serviceProvidersModel.getName().equals(""))
            serviceProvidersModel.setErrors(R.string.Name_Blank);

        /*if(serviceProvidersModel.getPhoneNumber().equals(""))
            serviceProvidersModel.setErrors(R.string.phoneNumber_Blank);
        else {
            if (serviceProvidersModel.getMobileNumber().length() != 10)
                serviceProvidersModel.setErrors(R.string.invalid_phone_number_length);
        }*/
        if (serviceProvidersModel.getEmailAddress().equals(""))
            serviceProvidersModel.setErrors(R.string.emailId_Blank);
        if (!(serviceProvidersModel.getEmailAddress().equals(""))) {
            boolean success=android.util.Patterns.EMAIL_ADDRESS.matcher(serviceProvidersModel.getEmailAddress()).matches();
            if(!success) {
                serviceProvidersModel.setErrors(R.string.invalid_email);
            }
            else
            {
                //if(dbOperations.checkEmailExists(serviceProvidersModel))
                    //serviceProvidersModel.setErrors(R.string.emailExists);
            }
        }
        if (serviceProvidersModel.getCity().equals(""))
            serviceProvidersModel.setErrors(R.string.cityBlank);

        if (!(serviceProvidersModel.getPhoneNumber().equals(""))) {
            if (serviceProvidersModel.getPhoneNumber().length() != 10)
                serviceProvidersModel.setErrors(R.string.invalid_phone_number_length);
        }
        else
        {
            serviceProvidersModel.setErrors(R.string.phoneNumber_Blank);
        }

        if (serviceProvidersModel.getPassword().equals(""))
            serviceProvidersModel.setErrors(R.string.password_Blank);
        if (serviceProvidersModel.getConfirmPassword().equals(""))
            serviceProvidersModel.setErrors(R.string.confirmPassword_Blank);
        if(serviceProvidersModel.getPassword().length()<8)
            serviceProvidersModel.setErrors(R.string.invalid_password_length);
        if(!((serviceProvidersModel.getPassword()).equals(serviceProvidersModel.getConfirmPassword())))
            serviceProvidersModel.setErrors(R.string.passwordMismatch);
    }
}

