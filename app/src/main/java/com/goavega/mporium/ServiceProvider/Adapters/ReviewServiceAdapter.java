package com.goavega.mporium.ServiceProvider.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.goavega.mporium.Model.RateModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Activities.ReviewServiceActivity;
import java.util.ArrayList;

/**
 * Created by Administrator on 13-04-2016.
 */
public class ReviewServiceAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    Context context;
    public ArrayList<RateModel> rateList= new ArrayList<RateModel>();

    public ReviewServiceAdapter(ReviewServiceActivity activity, ArrayList<RateModel> rateList) {
        layoutInflater=(LayoutInflater)activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rateList=rateList;
        this.context=activity;
    }

    @Override
    public int getCount() {
        return this.rateList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null) {
            TextView deviceNameText,
                    userNameText,
                    mobileNumberText,
                    commentText,
                    ratingText;
            RatingBar rateBar;
            listItem = layoutInflater.inflate(R.layout.serviced_device_display, null);
            deviceNameText=(TextView)listItem.findViewById(R.id.deviceNameText);
            userNameText=(TextView)listItem.findViewById(R.id.userNameText);
            mobileNumberText=(TextView)listItem.findViewById(R.id.mobileNumberText);
            commentText=(TextView)listItem.findViewById(R.id.commentText);
            //ratingText=(TextView)listItem.findViewById(R.id.ratingText);
            rateBar=(RatingBar)listItem.findViewById(R.id.ratingBar);
            deviceNameText.setText(this.rateList.get(position).getDeviceName());
            userNameText.setText(this.rateList.get(position).getUserName());
            String mobileNumber=this.rateList.get(position).getMobileNumber();
            if(!mobileNumber.equals("null"))
            mobileNumberText.setText(this.rateList.get(position).getMobileNumber());
           // ratingText.setText(this.rateList.get(position).getServiceProviderRates());
            rateBar.setRating(Float.parseFloat(this.rateList.get(position).getServiceProviderRates())) ;
            commentText.setText(this.rateList.get(position).getUserComment());
        }

        return listItem;
    }
}


