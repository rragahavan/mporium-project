package com.goavega.mporium.ServiceProvider.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.UserProfileTable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 30-03-2016.
 */
public class CustomerService {
    private Context context;
    ApiHelper apiHelper;
    UserProfileTable dbOperations;
    ApiUrls apiUrl;
    boolean isSaved=false,isInserted=false;
    ConnectionDetector connectionDetector;

    public CustomerService(Context context){
        this.context = context;
    }

    public boolean sendUserData(UserDetailModel userDetailModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        validate(userDetailModel);
        String userDetail;
        String message,uid;
        Long userId;

        if(!(userDetailModel.getHasErrors())) {

            JSONObject User = new JSONObject();
            try {
                User.put("name", userDetailModel.getName());
                //User.put("emailId", userDetailModel.getEmailAddress());
                User.put("mobileNumber",userDetailModel.getPhoneNumber());
                User.put("city", userDetailModel.getCity());
                User.put("addressLine1",userDetailModel.getAddressLine1());
                User.put("addressLine2",userDetailModel.getAddressLine2());
                //User.put("password", userDetailModel.getPassword());
                User.put("serviceProviderPresent",false);
                userDetail = User.toString();
                if(connectionDetector.isNetworkAvailable()) {
                    //today
                    //if(userDetailModel.getUserType().equals("serviceProvider"))
                    message = apiHelper.sendUserDetail(userDetail, context.getString(R.string.url)+apiUrl.addCustomerUrl);
                    User = new JSONObject(message);
                    if (User.getString("success").equals("true")) {
                        //isInserted = dbOperations.insertUser(userDetailModel);
                        //if (isInserted) {
                            /*SaveUserLogin.setUserName(context, userDetailModel.getEmailAddress());
                            SaveUserLogin.setUserId(context, User.getLong("userId"));
*/                            isSaved = true;
                        //}
                    } else {
                        //userDetailModel.setErrors(R.string.emailExists);
                        userDetailModel.setErrors(R.string.failure);
                    }
                }
                else {
                    userDetailModel.setErrors(R.string.network_faliure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }



    public UserDetailModel sendPhoneUserData(UserDetailModel userDetailModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        //validate(userDetailModel);
        String userDetail;
        String message,uid;
        Long userId;

        if(!(userDetailModel.getHasErrors())) {

            JSONObject user = new JSONObject();
            try {

                user.put("mobileNumber",userDetailModel.getPhoneNumber());
                userDetail = user.toString();
                if(connectionDetector.isNetworkAvailable()) {
                    message = apiHelper.getUserDetails(userDetail, context.getString(R.string.url) + apiUrl.searchCustomerUrl);
                    user = new JSONObject(message);
                    if (user.getString("success").equals("true"))
                    {
                        userDetailModel.setName(user.getString("customerName"));
                    userDetailModel.setCity(user.getString("city"));
                    if (!(user.getString("mobileNumber").equals("null")))
                        userDetailModel.setPhoneNumber(user.getString("mobileNumber"));
                    if (!(user.getString("address1").equals("null")))
                        userDetailModel.setAddressLine1(user.getString("address1"));
                    if (!(user.getString("address2").equals("null")))
                        userDetailModel.setAddressLine2(user.getString("address2"));
                }
                    else
                    {
                        userDetailModel.setErrors(R.string.user_not_exist);
                    }

                }
                else {
                    userDetailModel.setErrors(R.string.network_faliure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return userDetailModel;
    }


    public void validate(UserDetailModel userDetailModel) {
        userDetailModel.Errors.clear();
        if (userDetailModel.getName().equals(""))
            userDetailModel.setErrors(R.string.Name_Blank);
        if (userDetailModel.getCity().equals(""))
            userDetailModel.setErrors(R.string.cityBlank);
        /*if(userDetailModel.getAddressLine1().equals(""))
            userDetailModel.setErrors(R.string.addressLine1_Blank);
        if(userDetailModel.getAddressLine2().equals(""))
            userDetailModel.setErrors(R.string.addressLine2_Blank);*/
        if (userDetailModel.getPhoneNumber().equals(""))
            userDetailModel.setErrors(R.string.phoneNumber_Blank);
        if (!(userDetailModel.getPhoneNumber().equals("")))
            if(userDetailModel.getPhoneNumber().length()!=10)
                userDetailModel.setErrors(R.string.invalid_phone_number_length);

        /*if (userDetailModel.getEmailAddress().equals(""))
            userDetailModel.setErrors(R.string.emailId_Blank);
        if (!(userDetailModel.getEmailAddress().equals(""))) {
            boolean success=android.util.Patterns.EMAIL_ADDRESS.matcher(userDetailModel.getEmailAddress()).matches();
            if(!success) {
                userDetailModel.setErrors(R.string.invalid_email);
            }
            *//*else
            {
                if(dbOperations.checkEmailExists(userDetailModel))
                    userDetailModel.setErrors(R.string.emailExists);
            }*//*
        }*/

    }
}


