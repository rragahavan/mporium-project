package com.goavega.mporium.ServiceProvider.Activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.SearchView;

import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.DashBoardModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Adapters.ServiceProviderDashBoardAdapter;
import com.goavega.mporium.ServiceProvider.Model.ServiceProviderDashboardModel;
import com.goavega.mporium.ServiceProvider.Services.ServiceProviderDashboardService;
import com.goavega.mporium.Services.DashBoardService;
import com.goavega.mporium.adapters.DashBoardAdapter;
import com.goavega.mporium.db.AddDeviceTable;

import java.util.ArrayList;
import java.util.Date;

public class ServiceProviderDashboardDisplayActivity extends NavigationDrawerActivity {
    boolean f = true;
    private MenuItem searchMenuItem;
    AddDeviceTable addDeviceTable;
    Date sql;
    ServiceProviderDashboardModel dashBoardModel;
    ListView allNames;
    ServiceProviderDashboardService dashBoardService;
    ImageView dateFilterImage,
            menuIconImage,
            reviewService;
    private SearchView mSearchView;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    String userId;
    TextView custom_message;
    Toast toast;
    View layout;
    //SearchView searchView;

    ApiHelper apiHelper;
    ApiUrls apiUrl;
    String customerName;
    int datePickerMonthValue, datePickerYearValue;
    private ArrayList<ServiceProviderDashboardModel> dashBoardList = new ArrayList<ServiceProviderDashboardModel>();
    ArrayList<String> list;
    EditText searchText;
    LinearLayout linear;
    TextView dashboardTitle;
    Spinner searchFilterSpinner;
    String  filterOption="";
    LinearLayout linearSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_dashboard_display);
        menuIconImage = (ImageView) findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.service_provider_nav_drawer_item);
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.service_provider_nav_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        linearSpinner=(LinearLayout)findViewById(R.id.linearEditText);
        //searchView=(SearchView)findViewById(R.id.searchView);
        searchText=(EditText)findViewById(R.id.searchText);
        dashboardTitle=(TextView)findViewById(R.id.dashboardTitle);
        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        custom_message=(TextView)layout.findViewById(R.id.messageText);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoAddDevice = new Intent(getApplicationContext(), AddCustomerActivity.class);
                startActivity(gotoAddDevice);
            }
        });


        searchFilterSpinner=(Spinner)findViewById(R.id.searchFilter);

        ArrayAdapter<String> searchFilterAdapter=new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.search_filter));
        searchFilterSpinner.setAdapter(searchFilterAdapter);
        dashBoardModel = new ServiceProviderDashboardModel();
        dateFilterImage = (ImageView) findViewById(R.id.dateFilterImage);
        allNames = (ListView) findViewById(R.id.listNames);
        apiHelper = new ApiHelper(getApplicationContext());
        apiUrl = new ApiUrls();
        list = new ArrayList<String>();
        dashBoardService = new ServiceProviderDashboardService(ServiceProviderDashboardDisplayActivity.this);
       // userId = SaveUserLogin.getUserId(ServiceProviderDashboardDisplayActivity.this).toString();
        //addDeviceTable = new AddDeviceTable(DashboardDisplayActivity.this);
     //   DashBoardModel dashBoardModel = new DashBoardModel();
        dashBoardList = dashBoardService.setDeviceDetails();
        final ServiceProviderDashBoardAdapter adapter = new ServiceProviderDashBoardAdapter(ServiceProviderDashboardDisplayActivity.this, dashBoardList);
        allNames.setAdapter(adapter);


        dateFilterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //filterOption="";

                searchText.setVisibility(View.VISIBLE);
                searchText.requestFocus();
                dashboardTitle.setVisibility(View.GONE);
                searchFilterSpinner.setVisibility(View.VISIBLE);
                linearSpinner.setVisibility(View.VISIBLE);
                //ratingValue=
                searchFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
                {
                    String item=searchFilterSpinner.getSelectedItem().toString();
                    if(item.equals("Customer_Name"))
                    {
                        filterOption = "Customer_Name";
                    }
                    else if(item.equals("Customer_City"))
                    {
                        filterOption = "Customer_City";
                    }
                    else if(item.equals("Device_Name"))
                    {
                        filterOption = "Device_Name";
                    }
                    else
                    {
                        filterOption="Search by Filter";
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
              }

            );
            customerName=searchText.getText().toString();
                //customerName= (String) searchView.getQuery();
                if((!customerName.equals(""))&&(!filterOption.equals(""))&&(!filterOption.equals("Search by Filter"))) {
                    dashBoardList = dashBoardService.searchByCustomerName(customerName,filterOption);
                    final ServiceProviderDashBoardAdapter adapter = new ServiceProviderDashBoardAdapter(ServiceProviderDashboardDisplayActivity.this, dashBoardList);
                    allNames.setAdapter(adapter);
                }
            }
        });
    }
}
