package com.goavega.mporium.ApiDatas;

import android.content.Context;
import android.widget.Toast;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
/**
 * Created by RAKESH on 1/6/2016.
 */
public class ApiHelper {
    private Context context;

    public ApiHelper(Context context){
        this.context = context;
    }

    private void showToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public String sendUserDetail(String userDetail,String Url)
    {
        StringBuilder sb =new StringBuilder();
        URL url = null;
        try {
            url = new URL(Url);
            String message =userDetail;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //  conn.setReadTimeout(10000);
            // conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            conn.connect();
            OutputStream os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            os.flush();
            os.close();
            int responseCode = conn.getResponseCode();
            //showToast("responseCode"+responseCode);

            if(responseCode==201||responseCode==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                //sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }
            //  showToast("errors"+sb.toString());
            conn.disconnect();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return sb.toString();
    }

    public String getUserDetails(String userDetail,String Url)
    {
        URL url=null;
        StringBuilder sb =new StringBuilder();
        try {
            url=new URL(Url);
            String message=userDetail;
            //showToast("errors" + message);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setFixedLengthStreamingMode(message.getBytes().length);
            connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            connection.setConnectTimeout(1500);
            connection.setReadTimeout(1500);
            connection.connect();
            OutputStream os = new BufferedOutputStream(connection.getOutputStream());
            os.write(message.getBytes());
            os.flush();
            os.close();
            int status = connection.getResponseCode();
            if(status==201||status==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                // sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }
            //    showToast("errors" + sb.toString());

        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return sb.toString();
    }



    public String sendUpdateUserDetails(String userDetail,String Url)
    {
        StringBuilder sb =new StringBuilder();
        URL url = null;
        try {
            url = new URL(Url);
            String message =userDetail;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("PUT");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            conn.connect();
            OutputStream os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            os.flush();
            os.close();
            int responseCode = conn.getResponseCode();
            //showToast("responseCode"+responseCode);
            if(responseCode==201||responseCode==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                //sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }
            //showToast("errors"+sb.toString());
            conn.disconnect();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return sb.toString();
    }

    public String readAllUserDetails(String Url)
    {
        URL url;
        StringBuilder sb =new StringBuilder();
        HttpURLConnection c = null;
        try {
            url=new URL(Url);
            c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(1500);
            c.setReadTimeout(1500);
            c.connect();
            int status = c.getResponseCode();
            //showToast("responseCode"+status);
            if(status==201||status==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));

                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }

        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return sb.toString();
    }
}
