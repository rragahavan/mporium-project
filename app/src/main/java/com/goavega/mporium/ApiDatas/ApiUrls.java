package com.goavega.mporium.ApiDatas;

/**
 * Created by RAKESH on 1/7/2016.
 */
public class ApiUrls {

    public static final String signUpUrl="/users/saveUser";
    public static final String sendratingUrl="/serviceProviderRating/getServicedDetail";
    public static final String dashBoardDisplayUrl="/devices/dashboardCalculation";
    public static final String dateFilterUrl="/devices/dateFilter";
    public static final String saveRatingUrl="/serviceProviderRating/serviceRating";
    //public static final String loginUrl="/users/verifyUser";
    public static final String loginUrl="/users/verifyUser";
   // public static final String loginUrl="/usersLoginInfo/verifyUser";
    public static final String updateUrl="/users/updateUser";
    public static final String getUserUrl="/users/getUser";
    //public static final String getUserUrl="/usersLoginInfo/getUser";
    public static final String getManufacturerUrl="/manufacturers/getManufacturer";
    public static final String saveManufacturerUrl="/manufacturers/saveManufacturer";
    public static final String saveServiceProviderUrl="/serviceProviders/saveServiceProvider";
    public static final String getServiceProviderUrl="/serviceProviders/getServiceProvider";
    public static final String saveDeviceUrl="/devices/saveDevice";
    public static final String getDeviceUrl="/devices/getDevice";
    public static final String getDeviceId="/devices/deviceVerify";
    public static final String sendDeviceUrl="/devices/deviceList";
    public static final String warrantyUpdateUrl="/devices/updateWarrenty";
    public static final String serviceScheduleUpdateUrl="/devices/updateScheduleService";
    public static final String forgotPasswordUrl="/users/mailAction";
  // public static final String forgotPasswordUrl="/usersLoginInfo/mailAction";
    public static final String ratingUrl="/serviceProviderRating/serviceRating";
    public static final String getRatingUrl="/serviceProviderRating/getServicedDetail";
    public static final String sendTokenUrl="/pushNotification/notificationPush";
    //public static final String getNotificationUrl="/pushNotification/detailsNotification";
    public static final String getNotificationUrl="/pushNotification/detailsNotification";
    public static final String updateNotificationUrl="/pushNotification/updateNotificationPush";
    public static final String deleteDeviceUrl="/devices/deleteDevice";
    public static final String updateServiceProviderUrl="/serviceProviders/updateServiceProvider";
    public static final String serviceProviderSignUpUrl="/serviceProvidersLoginInfo/saveServiceProviderInfo";
    public static final String serviceProviderLoginUrl="/serviceProvidersLoginInfo/verifyServiceProvider";
    public static final String serviceProviderForgotPasswordUrl="/serviceProvidersLoginInfo/mailAction";
    public static final String serviceProviderUpdateUrl="/serviceProvidersLoginInfo/serviceProviderUpdate";
    public static final String serviceProviderDetailsUrl="/serviceProvidersLoginInfo/getServiceProvider";
    public static final String serviceProviderDashboardDisplayUrl="/devices/serviceProviderDashboard";
    //public static final String addCustomerUrl="/users/serviceProvidersCustomer";
    public static final String addCustomerUrl="/users/saveUser";
    public static final String searchCustomerUrl="/users/searchUserInServiceProvider";
    public static final String searchByCustomerNameUrl="/users/filterCustomer";
    public static final String getServiceProviderIdUrl="/devices/serviceProviderDashboardVerify";
    public static final String reviewServiceUrl ="/serviceProviderRating/viewCustomerRatings";
    public static final String searchRatingUrl="/serviceProviderRating/searchDeviceNameViewRatings";
    //public static final String customerCityUrl="/devices/customerCityFilter";
    //public static final String customerCityUrl="/devices/customerDeviceNameFilter";
    public static final String customerCityUrl="/devices/customerNameFilter";
}

