package com.goavega.mporium.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Created by RAKESH on 11/18/2015.
 */
public class MyDbOpenHelper extends SQLiteOpenHelper{
    public static final String userId="_id";
    public static final String firstName="firstName";
    public static final String lastName="lastName";
    public static final String emailAddress="emailAddress";
    public static final String addressLine1="addressLine1";
    public static final String addressLine2="addressLine2";
    public static final String city="city";
    public static final String phoneNumber="phoneNumber";
    public static final String password="password";
    public static final String userDetails="userDetails";

    //service provider table
    public static final String serviceProviderId="serviceProviderId";
    public static final String serviceProviderUserId="serviceProviderUserId";
    public static final String serviceProviderName="serviceProviderName";
    public static final String serviceProviderAddressLine1="serviceProviderAddressLine1";
    public static final String serviceProviderAddressLine2="serviceProviderAddressLine2";
    public static final String serviceProviderCity="serviceProviderCity";
    public static final String serviceProviderPhoneNumber="serviceProviderPhoneNumber";
    public static final String serviceProviderContactPerson="serviceProviderContactPerson";
    public static final String serviceProviderDetails="serviceProviderDetails";

    //manufacturer details
    public static final String manufacturerId="manufacturerId";
    public static final String manufacturerName="manufacturerName";
    public static final String manufacturerCity="manufacturerCity";
    public static final String manufacturerPhoneNumber="manufacturerPhoneNumber";
    public static final String manufacturerDetails="manufacturerDetails";

    public static final String DEVICE_ID="device_id";
    public static final String deviceId="deviceId";
    public static final String deviceUserId="deviceUserId";
    public static final String DEVICE_NAME="device_name";
    public static final String PURCHASE_DATE="purchase_date";
    public static final String MODEL_NUMBER="model_number";
    public static final String SERIAL_NUMBER="serial_number";
    public static final String WARRANTY_PERIOD="warranty_period";
    public static final String MANUFACTURER="manufacturer";
    public static final String SERVICEPROVIDER="service_provider";
    public static final String SERVICE_SCHEDULE="service_schedule";
    public static final String SERVICE_SCHEDULE_DATE="service_schedule_date";
    public static final String SERVICE_REMAINING_DAYS="service_remaining_days";
    public static final String WARRANTY_SCHEDULE_DATE="warranty_schedule_date";
    public static final String WARRANTY_REMAINING_YEARS="warranty_remaining_years";
    public static final String WARRANTY_REMAINING_DAYS="warranty_remaining_days";
    public static final String WARRANTY_REMAINING_MONTHS="warranty_remaining_months";
    public static final String DEVICE_DETAILS="device_details";

    public MyDbOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("Table created", "Database Table is created");
        db.execSQL("CREATE TABLE userDetails(_id INTEGER PRIMARY KEY AUTOINCREMENT,firstName TEXT,lastName TEXT,emailAddress TEXT,addressLine1 TEXT,addressLine2 TEXT,city TEXT,phoneNumber TEXT,password TEXT)");
        db.execSQL("CREATE TABLE manufacturerDetails(manufacturerId INTEGER PRIMARY KEY AUTOINCREMENT,manufacturerName TEXT,manufacturerCity TEXT,manufacturerPhoneNumber TEXT)");
        db.execSQL("CREATE TABLE serviceProviderDetails(serviceProviderId INTEGER PRIMARY KEY AUTOINCREMENT,serviceProviderUserId TEXT,serviceProviderName TEXT,serviceProviderCity TEXT,serviceProviderPhoneNumber TEXT)");
        db.execSQL("CREATE TABLE device_details(device_id INTEGER PRIMARY KEY AUTOINCREMENT,deviceUserId LONG,device_name TEXT,deviceId LONG,purchase_date Text,model_number TEXT,serial_number TEXT,warranty_period TEXT,manufacturer TEXT,service_provider TEXT,service_schedule TEXT,service_schedule_date TEXT,service_remaining_days TEXT,warranty_schedule_date TEXT,warranty_remaining_years TEXT,warranty_remaining_days TEXT,warranty_remaining_Months TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}