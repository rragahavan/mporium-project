package com.goavega.mporium.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.goavega.mporium.Model.ManufacturerDetailModel;
import com.goavega.mporium.R;

/**
 * Created by RAKESH on 11/27/2015.
 */
public class ManufacturerTable {
    SQLiteDatabase db;
    MyDbOpenHelper openHelper;
    public ManufacturerTable(Context context) {
        String dbName=context.getString(R.string.database_name);
        int dbVersion=context.getResources().getInteger(R.integer.database_version);
        openHelper=new MyDbOpenHelper(context,dbName,null,dbVersion);
    }

    public boolean insertManufacturer(ManufacturerDetailModel manufacturerDetailModel)
    {

        db=openHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(MyDbOpenHelper.manufacturerName,manufacturerDetailModel.getName());
        cv.put(MyDbOpenHelper.manufacturerCity,manufacturerDetailModel.getCity());
        cv.put(MyDbOpenHelper.manufacturerPhoneNumber,manufacturerDetailModel.getPhoneNumber());
        long id=db.insert(MyDbOpenHelper.manufacturerDetails,null,cv);
        db.close();
        if(id<0)
        {
            return false;
        }
        return true;
    }
    public Cursor readAllManufacturer()
    {
        db=openHelper.getReadableDatabase();
        Cursor mf=db.query(openHelper.manufacturerDetails,null,null,null,null,null,null);
        return mf;
    }
}


