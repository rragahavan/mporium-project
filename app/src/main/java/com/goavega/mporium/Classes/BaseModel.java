package com.goavega.mporium.Classes;

import java.util.ArrayList;

/**
 * Created by RAKESH on 1/11/2016.
 */
public class BaseModel

{
    public ArrayList<Integer> Errors;
    public BaseModel()
    {
        Errors=new ArrayList<Integer>();
    }

    public void setErrors(Integer Error)
    {
        Errors.add(Error);
    }

    public  ArrayList<Integer> getErrors()
    {
        return Errors;
    }

    public boolean getHasErrors()
    {
        return (this.Errors!=null && this.Errors.size()>0);
    }
}
