package com.goavega.mporium.Classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by RAKESH on 1/24/2016.
 */
public class SaveUserLogin {
    static final String PREF_USER_NAME= "username";
    static final String PREF_USER_ID= "userId";
    static final String SERVICE_PROVIDER_NAME= "serviceProviderName";

    static final String PREF_SERVICE_PROVIDER_ID= "serviceProviderId";
    static final String PREF_USER_EMAIL= "userEmail";

    public static String getServiceProviderName(Context ctx) {

        return getSharedPreferences(ctx).getString(SERVICE_PROVIDER_NAME, "");

    }


    public static String getServiceProviderEmail(Context ctx) {

        return getSharedPreferences(ctx).getString(SERVICE_PROVIDER_EMAIL, "");

    }

    static final String SERVICE_PROVIDER_EMAIL= "serviceProviderEmail";
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static Long getPrefServiceProviderId(Context ctx) {
        return getSharedPreferences(ctx).getLong(PREF_SERVICE_PROVIDER_ID, 0);
    }

    public static void setUserId(Context ctx, Long userId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putLong(PREF_USER_ID, userId);
        editor.commit();
    }

    public static void setPrefServiceProviderId(Context ctx, Long serviceProviderId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putLong(PREF_SERVICE_PROVIDER_ID, serviceProviderId);
        editor.commit();
    }


    public static Long getUserId(Context ctx)
    {
        return getSharedPreferences(ctx).getLong(PREF_USER_ID, 0);
    }
        public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }
    public static void setServiceProviderName(Context ctx,String serviceProviderName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(SERVICE_PROVIDER_NAME, serviceProviderName);
        editor.commit();

    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static String getPrefUserEmail(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_EMAIL, "");
    }

    public static void setPrefUserEmail(Context ctx,String userEmail) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_EMAIL, userEmail);
        editor.commit();
    }
    public static void setServiceProviderEmail(Context ctx,String serviceProviderEmail)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(SERVICE_PROVIDER_EMAIL, serviceProviderEmail);
        editor.commit();
    }
}
