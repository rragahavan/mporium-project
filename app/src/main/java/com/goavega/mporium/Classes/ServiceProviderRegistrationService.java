package com.goavega.mporium.Classes;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.goavega.mporium.R;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Administrator on 18-04-2016.
 */
public class ServiceProviderRegistrationService extends IntentService {
    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private static String tokens=null;
    // public static final String sendTokenUrl="http://gvg-u1.cloudapp.net:8073/mporium/pushNotification/notificationPush";
    //public static final String sendTokenUrl="http://192.168.1.123:8001/pushNotification/notificationPush";
    public static final String sendTokenUrl="http://192.168.1.134:8081/Mporium/pushNotification/notificationPush";
    public static final String sendServiceProviderTokenUrl="http://192.168.1.123:8081/Mporium/pushNotification/notificationPush";

    //public static final String sendTokenUrl="http://192.168.1.123:9999/pushNotification/notificationPush";
    Integer days;
    Long userId;
    Long serviceProviderId;
    public ServiceProviderRegistrationService() {

        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
//            String msg=intent.getStringExtra("notification");
            days=intent.getIntExtra("days", 0);
            userId=intent.getLongExtra("userId",0);
            serviceProviderId = intent.getLongExtra("serviceProviderId", 0);

            sendRegistrationToServer(token);
            subscribeTopics(token);
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
        } catch (Exception e) {
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
        }

        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

    }

    private void sendRegistrationToServer(String token) {
        JSONObject UserToken = new JSONObject();
        String tokenDetails;
        try {
            UserToken.put("registerId", new Long(getString(R.string.gcm_defaultSenderId)));
            UserToken.put("token",token);
            UserToken.put("notifyMeBefore",days);
            UserToken.put("notification","true");
            //UserToken.put("user",userId);
            UserToken.put("user",1L);
            //if(SaveUserLogin.getPrefServiceProviderId(RegistrationIntentService.this) != 0) {
            UserToken.put("serviceProvider",serviceProviderId);
            //}


            tokenDetails = UserToken.toString();
            sendUserDetail(tokenDetails, sendTokenUrl);
            //sendUserDetail(tokenDetails,sendServiceProviderTokenUrl);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    public String sendUserDetail(String userDetail,String Url)
    {
        StringBuilder sb = null;
        URL url = null;
        try {
            url = new URL(Url);
            String message =userDetail;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            conn.connect();
            OutputStream os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            os.flush();
            os.close();
            int responseCode = conn.getResponseCode();
            if(responseCode==201||responseCode==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
            }
            conn.disconnect();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return sb.toString();
    }
}
