package com.goavega.mporium.Model;

import com.goavega.mporium.Classes.BaseModel;

import java.sql.Date;

/**
 * Created by Administrator on 08-02-2016.
 */
public class RateModel extends BaseModel{
    float serviceProviderRate;
    String userComment;
    Long userId;
    String serviceProviderName;
    Long deviceId;
    Long serviceProviderId;

    public String getServiceProviderRates() {
        return serviceProviderRates;
    }

    public void setServiceProviderRates(String serviceProviderRates) {
        this.serviceProviderRates = serviceProviderRates;
    }

    String serviceProviderRates;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    String userName;
    String mobileNumber;
    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    String serviceDate;

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    String manufacturerName;
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    String deviceName;

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }


    public Date getServicedDate() {
        return servicedDate;
    }

    public void setServicedDate(Date servicedDate) {
        this.servicedDate = servicedDate;
    }

    Date servicedDate;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public float getServiceProviderRate() {
        return serviceProviderRate;
    }

    public void setServiceProviderRate(float serviceProviderRate) {
        this.serviceProviderRate = serviceProviderRate;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Long serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

}
